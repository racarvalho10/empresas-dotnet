-- Desafio IOASYS

## Sobre a aplicação

Trata-se de uma API para Cadastro de usuarios e filmes

## Tecnologias utilizadas

O Projeto é desenvolvido e mantido nas seguintes tecnologias:

*DOTNET CORE
*ENTITY FRAMEWORK
\*SQLSERVER

## Arquitetura e organização do projeto

O projeto esta organizado na seguinte arvore de pastas:

\*SRC

- /Desafio.Api.Core
- /Desafio.Api.Data
- /Desafio.Api.Domain
- /Desafio.Api.Host
- /Desafio.Api.Service

### executando a aplicação pela primeira vez

Para executar a aplicação pela primeira vez precisa ir até a pasta **src** e executar os seguintes comandos:  
** dotnet restore
** dotnet build

Após:

Acessar a pasta **/Desafio.Api** abrir o Visual Studio (Code..) apertar F5, ele já irá rodar o projeto abrindo a URL para testar
no **Swagger**, não esquecer de realizar o procedimento abaixo antes de dar o F5:

**_Criei o banco no docker_ SQLSERVER**
Criando a base local para desenvolvimento somente trocar a connectionString nos arquivos "launch.json" e "RepositoryDbContextFactory.cs" e dar F5 na aplicação (projeto) ele ja vai criar o banco e rodar as migrations que esta no projeto.

Após:

### Documentação dos metodos da API

A documentação dos endpoints da API podem ser consultadas no **Swagger** através do endereço **{URL}/index.html**

-- não consegui acabar o projeto por inteiro, realmente esta muito corrido no trabalho, faltou gerar o token, estava finalizando, deixei comentado e realizar a votação dos filmes--
