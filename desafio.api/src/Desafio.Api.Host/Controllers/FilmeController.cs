using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Desafio.Api.Core.Entities;
using Desafio.Api.Domain.Entities;
using Desafio.Api.Domain.Interface.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Desafio.Api.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class FilmeController : ControllerBase
  {

    public FilmeController()
    {
    }

    [HttpGet()]
    [AllowAnonymous]
    public async Task<ActionResult> Get([FromServices] IFilmeService serviceFilme)
    {
      if (ModelState.IsValid)
      {
        var result = await serviceFilme.GetAllAsync();
        return Ok(result);
      }
      return BadRequest(ModelState);

    }


    [HttpGet("{id}", Name = "GetFilmeById")]
    [AllowAnonymous]
    public async Task<ActionResult> GetById([FromServices] IFilmeService serviceFilme, [FromRoute][Required] Guid id)
    {
      if (ModelState.IsValid)
      {
        var result = await serviceFilme.GetByIdAsync(id);

        if (result == null)
          return NotFound(new { id });
        else
          return Ok(result);
      }
      return BadRequest(ModelState);

    }

    [HttpPost()]
    [AllowAnonymous]
    public async Task<ActionResult> Post([FromServices] IFilmeService filmeService, [FromBody] Filme model)
    {
      if (ModelState.IsValid)
      {
        var result = await filmeService.AddAsync(model);
        return CreatedAtRoute("GetFilmeById", new { id = result.Id }, result);
      }
      return BadRequest(ModelState);
    }



    [HttpPut("{id}")]
    [AllowAnonymous]
    public async Task<ActionResult> Put([FromServices] IFilmeService filmeService, [FromRoute][Required] Guid id, [FromBody] Filme model)
    {
      if (ModelState.IsValid)
      {
        var result = await filmeService.UpdateAsync(model, id);

        if (result == null)
          return NotFound(new { id });

        return Ok(result);
      }
      return BadRequest(ModelState);
    }


    [HttpDelete("{id}")]
    [AllowAnonymous]
    public async Task<ActionResult> Delete([FromServices] IFilmeService filmeService, [FromRoute][Required] Guid id)
    {
      if (ModelState.IsValid)
      {
        var result = await filmeService.DeleteAsync(id);
        if (result == 0) return NotFound(new { id }); else return Ok();
      }
      return BadRequest(ModelState);
    }
  }
}