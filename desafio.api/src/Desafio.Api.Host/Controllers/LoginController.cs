using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Desafio.Api.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using System.Net;
using Desafio.Api.Domain.Interface.Repositories;


namespace Desafio.Api.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class LoginController : ControllerBase
  {
    [AllowAnonymous]
    [HttpPost]
    public async Task<object> Login([FromBody] Usuario usuario, [FromServices] ILoginService service)
    {
      if (!ModelState.IsValid)
        return BadRequest(ModelState);

      if (usuario == null)
        return BadRequest();
      try
      {
        var result = await service.FindByLogin(usuario);

        if (result != null)
          return Ok(result);
        else
          return NotFound();
      }
      catch (ArgumentException ex)
      {
        return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
      }
    }


    // [HttpPost()]
    // [AllowAnonymous]
    // public async Task<ActionResult> Post([FromServices] IUsuarioService usuarioService, [FromBody] Usuario model)
    // {
    //   if (ModelState.IsValid)
    //   {
    //     var result = await usuarioService.AddAsync(model);
    //     return CreatedAtRoute("GetUsuarioById", new { id = result.Id }, result);
    //   }
    //   return BadRequest(ModelState);
    // }


    [HttpGet]
    public async Task<IActionResult> Get()
    {
      return Ok("Ok");
    }
  }
}
