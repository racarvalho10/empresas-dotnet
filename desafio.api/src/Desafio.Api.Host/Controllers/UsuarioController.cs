using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Desafio.Api.Core.Entities;
using Desafio.Api.Domain.Entities;
using Desafio.Api.Domain.Interface.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Desafio.Api.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class UsuarioController : ControllerBase
  {

    public UsuarioController()
    {
    }

    [HttpGet()]
    [AllowAnonymous]
    public async Task<ActionResult> Get([FromServices] IUsuarioService serviceUsuario)
    {
      if (ModelState.IsValid)
      {
        var result = await serviceUsuario.GetAllAsync();
        return Ok(result);
      }
      return BadRequest(ModelState);

    }


    [HttpGet("{id}", Name = "GetUsuarioById")]
    [AllowAnonymous]
    public async Task<ActionResult> GetById([FromServices] IUsuarioService serviceUsuario, [FromRoute][Required] Guid id)
    {
      if (ModelState.IsValid)
      {
        var result = await serviceUsuario.GetByIdAsync(id);

        if (result == null)
          return NotFound(new { id });
        else
          return Ok(result);
      }
      return BadRequest(ModelState);

    }

    [HttpPost()]
    [AllowAnonymous]
    public async Task<ActionResult> Post([FromServices] IUsuarioService usuarioService, [FromBody] Usuario model)
    {
      if (ModelState.IsValid)
      {
        var result = await usuarioService.AddAsync(model);
        return CreatedAtRoute("GetUsuarioById", new { id = result.Id }, result);
      }
      return BadRequest(ModelState);
    }


    // [HttpPost()]
    // [Route("login")]
    // // [AllowAnonymous]
    // // public Task<object> Authenticate([FromBody] Usuario model)
    // // {

    //   [AllowAnonymous]
    //     [HttpPost]
    //     public async Task<object> Login([FromBody] LoginDto loginDto, [FromServices] ILoginService service)
    //     {
    //         if (!ModelState.IsValid)
    //             return BadRequest(ModelState);

    //         if (loginDto == null)
    //             return BadRequest();
    //         try
    //         {
    //             var result = await service.FindByLogin(loginDto);

    //             if (result != null)
    //                 return Ok(result);
    //             else
    //                 return NotFound();
    //         }
    //         catch (ArgumentException ex)
    //         {
    //             return StatusCode((int)HttpStatusCode.InternalServerError, ex.Message);
    //         }


    //     }

    //   // var user = await usuarioService.Usuario
    //   //                                .AsNotracking()
    //   //                                .Where(x => x.Username == model.Username && x.Password == model.Password)
    //   //                                .FirstOrDefaultAsync();

    //   // if (user == null)
    //   // {
    //   //   return NotFound(new { message = "Usuario ou senha inválida" });
    //   // }

    //   // var token = TokenService.GenerateToken(user);

    //   // user.Password = "";
    //   // return new
    //   // {
    //   //   user = user,
    //   //   token = token
    //   // };
    // }





    [HttpPut("{id}")]
    [AllowAnonymous]
    public async Task<ActionResult> Put([FromServices] IUsuarioService usuarioService, [FromRoute][Required] Guid id, [FromBody] Usuario model)
    {
      if (ModelState.IsValid)
      {
        var result = await usuarioService.UpdateAsync(model, id);

        if (result == null)
          return NotFound(new { id });

        return Ok(result);
      }
      return BadRequest(ModelState);
    }


    [HttpDelete("{id}")]
    [AllowAnonymous]
    public async Task<ActionResult> Delete([FromServices] IUsuarioService usuarioService, [FromRoute][Required] Guid id)
    {
      if (ModelState.IsValid)
      {
        var result = await usuarioService.DeleteAsync(id);
        if (result == 0) return NotFound(new { id }); else return Ok();
      }
      return BadRequest(ModelState);
    }
  }
}