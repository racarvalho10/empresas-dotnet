using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Desafio.Api.Core.Entities;
using Desafio.Api.Domain.Entities;
using Desafio.Api.Domain.Interface.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Desafio.Api.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class AdministradorController : ControllerBase
  {

    public AdministradorController()
    {
    }

    [HttpGet()]
    [AllowAnonymous]
    public async Task<ActionResult> Get([FromServices] IAdministradorService serviceAdministrador)
    {
      if (ModelState.IsValid)
      {
        var result = await serviceAdministrador.GetAllAsync();
        return Ok(result);
      }
      return BadRequest(ModelState);

    }

    [HttpGet("{id}", Name = "GetAdministradorById")]
    [AllowAnonymous]
    public async Task<ActionResult> GetById([FromServices] IAdministradorService administradorService, [FromRoute][Required] Guid id)
    {
      if (ModelState.IsValid)
      {
        var result = await administradorService.GetByIdAsync(id);

        if (result == null)
          return NotFound(new { id });
        else
          return Ok(result);
      }
      return BadRequest(ModelState);

    }


    [HttpPost()]
    [AllowAnonymous]
    public async Task<ActionResult> Post([FromServices] IAdministradorService administradorService, [FromBody] Administrador model)
    {
      if (ModelState.IsValid)
      {
        var result = await administradorService.AddAsync(model);
        return CreatedAtRoute("GetAdministradorById", new { id = result.Id }, result);
      }
      return BadRequest(ModelState);
    }


    [HttpPut("{id}")]
    [AllowAnonymous]
    public async Task<ActionResult> Put([FromServices] IAdministradorService administradorService, [FromRoute][Required] Guid id, [FromBody] Administrador model)
    {
      if (ModelState.IsValid)
      {
        var result = await administradorService.UpdateAsync(model, id);

        if (result == null)
          return NotFound(new { id });

        return Ok(result);
      }
      return BadRequest(ModelState);
    }


    [HttpDelete("{id}")]
    [AllowAnonymous]
    public async Task<ActionResult> Delete([FromServices] IAdministradorService administradorService, [FromRoute][Required] Guid id)
    {
      if (ModelState.IsValid)
      {
        var result = await administradorService.DeleteAsync(id);
        if (result == 0) return NotFound(new { id }); else return Ok();
      }
      return BadRequest(ModelState);
    }
  }
}
