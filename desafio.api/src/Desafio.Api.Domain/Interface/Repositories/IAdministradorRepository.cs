using Desafio.Api.Domain.Entities;

namespace Desafio.Api.Domain.Interface.Repositories
{
  public interface IAdministradorRepository : IRepositoryBase<Administrador>
  {
  }
}