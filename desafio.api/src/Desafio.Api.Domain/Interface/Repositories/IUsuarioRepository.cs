using Desafio.Api.Domain.Entities;

namespace Desafio.Api.Domain.Interface.Repositories
{
  public interface IUsuarioRepository : IRepositoryBase<Usuario>
  {
  }
}