using Desafio.Api.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Desafio.Api.Domain.Interface.Repositories
{
  public interface ILoginService
  {
    Task<object> FindByLogin(Usuario user);
  }
}