using Desafio.Api.Domain.Entities;

namespace Desafio.Api.Domain.Interface.Repositories
{
  public interface IFilmeRepository : IRepositoryBase<Filme>
  {
  }
}