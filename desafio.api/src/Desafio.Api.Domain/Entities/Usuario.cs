using System;

namespace Desafio.Api.Domain.Entities
{
  public class Usuario
  {
    public Guid Id { get; set; }
    public string Username { get; set; }
    public string email { get; set; }
    public string Password { get; set; }
    public string TipoUsuario { get; set; }


  }
}