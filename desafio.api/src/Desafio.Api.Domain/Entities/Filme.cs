using System;

namespace Desafio.Api.Domain.Entities
{
  public class Filme
  {
    public Guid Id { get; set; }
    public string Nome { get; set; }
    public string Detalhes { get; set; }

  }
}