using System;

namespace Desafio.Api.Domain.Entities
{
  public class Administrador
  {
    public Guid Id { get; set; }
    public string Username { get; set; }
    public string email { get; set; }
    public string Password { get; set; }
  }
}