using Desafio.Api.Domain.Entities;
using Desafio.Api.Domain.Interface.Repositories;
using Desafio.Api.Domain.Interface.Services;

namespace Desafio.Api.Service.FilmeService
{
  public class FilmeService : ServiceBase<Filme>, IFilmeService
  {
    private readonly IFilmeRepository _repository;

    public FilmeService(IFilmeRepository repository) : base(repository)
    {
      _repository = repository;
    }
  }
}