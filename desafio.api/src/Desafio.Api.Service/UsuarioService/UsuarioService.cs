using Desafio.Api.Domain.Entities;
using Desafio.Api.Domain.Interface.Repositories;
using Desafio.Api.Domain.Interface.Services;

namespace Desafio.Api.Service.UsuarioService
{
  public class UsuarioService : ServiceBase<Usuario>, IUsuarioService
  {
    private readonly IUsuarioRepository _repository;

    public UsuarioService(IUsuarioRepository repository) : base(repository)
    {
      _repository = repository;
    }
  }
}