using Desafio.Api.Domain.Entities;
using Desafio.Api.Domain.Interface.Repositories;
using Desafio.Api.Domain.Interface.Services;

namespace Desafio.Api.Service.AdministradorService
{
  public class AdministradorService : ServiceBase<Administrador>, IAdministradorService
  {
    private readonly IAdministradorRepository _repository;

    public AdministradorService(IAdministradorRepository repository) : base(repository)
    {
      _repository = repository;
    }
  }
}