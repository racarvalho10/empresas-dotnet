using Desafio.Api.Domain.Interface.Repositories;
using Desafio.Api.Domain.Entities;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Desafio.Api.Domain.Security;


namespace Desafio.Api.Service.LoginService
{
  public class LoginService : ILoginService
  {

    private SigningConfigurations _signingConfigurations;
    private TokenConfigurations _tokenConfigurations;
    private IConfiguration _configuration { get; }


    public LoginService(SigningConfigurations signingConfigurations,
                            TokenConfigurations tokenConfigurations,
                            IConfiguration configuration)
    {
      _signingConfigurations = signingConfigurations;
      _tokenConfigurations = tokenConfigurations;
      _configuration = configuration;
    }

    public async Task<object> FindByLogin(Usuario user)
    {
      var baseUser = new Usuario();
      if (user != null && !string.IsNullOrWhiteSpace(user.Username))
      {
        // baseUser = await _repository.FindByLogin(user.Username);
        // if (baseUser == null)
        // {
        //     return new
        //     {
        //         authenticated = false,
        //         message = "Falha ao autenticar"
        //     };
        // }
        // else
        // {
        var identity = new ClaimsIdentity(
            new GenericIdentity(user.Username),
            new[]
            {
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                            new Claim(JwtRegisteredClaimNames.UniqueName, user.Username),
            }
        );

        var createDate = DateTime.UtcNow;
        var expirationDate = createDate + TimeSpan.FromSeconds(_tokenConfigurations.Seconds);

        var handler = new JwtSecurityTokenHandler();
        var token = CreateToken(identity, createDate, expirationDate, handler);
        return SuccessObject(createDate, expirationDate, token, baseUser);
        //}
      }
      else
      {
        return new
        {
          authenticated = false,
          message = "Falha ao autenticar"
        };
      }
    }

    private string CreateToken(ClaimsIdentity identity, DateTime createDate, DateTime expirationDate, JwtSecurityTokenHandler handler)
    {
      var securityToken = handler.CreateToken(new SecurityTokenDescriptor
      {
        Issuer = _tokenConfigurations.Issuer,
        Audience = _tokenConfigurations.Audience,
        SigningCredentials = _signingConfigurations.SigningCredentials,
        Subject = identity,
        NotBefore = createDate,
        Expires = expirationDate,
      });

      var token = handler.WriteToken(securityToken);
      return token;
    }

    private object SuccessObject(DateTime createDate, DateTime expirationDate, string token, Usuario user)
    {
      return new
      {
        authenticated = true,
        create = createDate.ToString("yyyy-MM-dd HH:mm:ss"),
        expiration = expirationDate.ToString("yyyy-MM-dd HH:mm:ss"),
        accessToken = token,
        userName = user.Username,
        // name = user.Name,
        message = "Usuário Logado com sucesso"
      };
    }

  }
}