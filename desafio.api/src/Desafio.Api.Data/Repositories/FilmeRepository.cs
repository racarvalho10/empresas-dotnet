using Desafio.Api.Data.Context;
using Desafio.Api.Domain.Entities;
using Desafio.Api.Domain.Interface.Repositories;

namespace Desafio.Api.Data.Repositories
{
  public class FilmeRepository : RepositoryBase<Filme>, IFilmeRepository
  {
    readonly RepositoryDbContext _repositoryDbContext;

    public FilmeRepository(RepositoryDbContext repositoryDbContext) : base(repositoryDbContext)
    {
      _repositoryDbContext = repositoryDbContext;
    }
  }
}