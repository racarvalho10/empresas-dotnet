using Desafio.Api.Data.Context;
using Desafio.Api.Domain.Entities;
using Desafio.Api.Domain.Interface.Repositories;

namespace Desafio.Api.Data.Repositories
{
  public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
  {
    readonly RepositoryDbContext _repositoryDbContext;

    public UsuarioRepository(RepositoryDbContext repositoryDbContext) : base(repositoryDbContext)
    {
      _repositoryDbContext = repositoryDbContext;
    }
  }
}