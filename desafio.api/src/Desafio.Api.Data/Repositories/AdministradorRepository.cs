using Desafio.Api.Data.Context;
using Desafio.Api.Domain.Entities;
using Desafio.Api.Domain.Interface.Repositories;

namespace Desafio.Api.Data.Repositories
{
  public class AdministradorRepository : RepositoryBase<Administrador>, IAdministradorRepository
  {
    readonly RepositoryDbContext _repositoryDbContext;

    public AdministradorRepository(RepositoryDbContext repositoryDbContext) : base(repositoryDbContext)
    {
      _repositoryDbContext = repositoryDbContext;
    }
  }
}