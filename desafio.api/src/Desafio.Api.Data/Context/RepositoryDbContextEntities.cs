using Desafio.Api.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Desafio.Api.Data.Context
{
  public partial class RepositoryDbContext : DbContext
  {
    public DbSet<Administrador> Administrador { get; set; }
    public DbSet<Usuario> Usuario { get; set; }
    public DbSet<Filme> Filme { get; set; }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<Administrador>();
      modelBuilder.Entity<Usuario>();
      modelBuilder.Entity<Filme>();
      // modelBuilder.Entity<Filme>(entity =>
      // {
      //   entity.HasOne<Escola>(o => o.EscolaReference)
      //           .WithMany()
      //           .HasForeignKey(o => o.EscolaId)
      //           .HasPrincipalKey(d => d.Id)
      //           .IsRequired(true)
      //           .OnDelete(DeleteBehavior.Restrict);
      // });

      base.OnModelCreating(modelBuilder);
    }
  }
}